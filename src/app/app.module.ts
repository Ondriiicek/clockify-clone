import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularFirestoreModule} from "@angular/fire/compat/firestore";
import {AngularFireModule} from "@angular/fire/compat";
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import {ToastModule} from 'primeng/toast';
import {DropdownModule} from 'primeng/dropdown';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AddNewComponent } from './components/add-new/add-new.component';
import { ActivityListComponent } from './components/task-list/activity-list.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { ActivityHeaderComponent } from './components/task-list/list-header/activity-header.component';
import {InplaceModule} from "primeng/inplace";
import { EditComponent } from './components/task-list/edit/edit.component';
import { HighlightDirective } from './components/add-new/highlight.directive';
import { FilterPipe } from './components/task-list/filter.pipe';
import { ActivityComponent } from './components/task-list/activity/activity.component';
import { NoActivityComponent } from './components/no-activity/no-activity.component';

@NgModule({
  declarations: [
    AppComponent,
    AddNewComponent,
    ActivityListComponent,
    ActivityHeaderComponent,
    EditComponent,
    HighlightDirective,
    FilterPipe,
    ActivityComponent,
    NoActivityComponent
  ],
  imports: [
    BrowserModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    CalendarModule,
    BrowserAnimationsModule,
    ToastModule,
    InplaceModule,
    DropdownModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
