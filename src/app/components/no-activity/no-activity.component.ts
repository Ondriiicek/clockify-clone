import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-activity',
  templateUrl: './no-activity.component.html',
  styleUrls: ['./no-activity.component.css']
})
export class NoActivityComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
