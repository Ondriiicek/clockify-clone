import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoActivityComponent } from './no-activity.component';

describe('NoActivityComponent', () => {
  let component: NoActivityComponent;
  let fixture: ComponentFixture<NoActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
