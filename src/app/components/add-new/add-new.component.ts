import {Component, OnInit} from '@angular/core';
import {AddNewService} from "./add-new.service";
import {SharedService} from "../shared.service";
import {Subject, takeUntil, timer} from "rxjs"
import {FormGroup} from "@angular/forms";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css'],
  providers: [AddNewService,
              SharedService,
              MessageService]
})
export class AddNewComponent implements OnInit {

  isManual : boolean = true;
  isStarted : boolean = false;
  newActivityForm! : FormGroup;
  sub$ = new Subject<boolean>();
  isBillable : boolean = false;

  constructor( private addNewService : AddNewService,
               private sharedService : SharedService ) { }

  ngOnInit(): void {
    this.newActivityForm = this.addNewService.initForm();
  }

  onAdd(){
    this.addNewService.addNewActivity( this.newActivityForm );
    this.newActivityForm.get("description")?.reset();
    this.newActivityForm.get("activeTime")?.reset();
    this.isBillable = false;
  }

  onStart(){
    this.isStarted = true;

    timer(1000,1000).pipe(
      takeUntil(this.sub$)
    ).subscribe(val => {
      this.newActivityForm = this.addNewService.timerLogic(this.newActivityForm, val);
    });
  }

  onStop(){
    this.onAdd();
    this.isStarted = false;
    this.sub$.next(true);
    this.sub$.unsubscribe();
  }

  //Time defference between start and end
  countDifference(){
    const difference = this.sharedService.countActiveTime( this.newActivityForm );
    this.newActivityForm.get("activeTime")?.setValue(difference);
  }

  onSetActiveTime(){
    const newEndTime = this.sharedService.onChangeActiveTime(this.newActivityForm);
    this.newActivityForm.get("endTime")?.setValue(newEndTime);
  }

  onBillable(){
    this.isBillable = !this.isBillable;
    this.newActivityForm.get("isBillable")?.setValue(this.isBillable);
  }

  toManual(){
    this.isManual = true;
  }

  toTimer(){
    this.isManual = false;
    this.newActivityForm.get("startTime")?.setValue(new Date());
    this.newActivityForm.get("activeTime")?.setValue(new Date(0 - 3600*1000))
    this.newActivityForm.get("endTime")?.setValue(new Date());
  }

}
