import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @HostBinding('style.color') billing : any;
  isSelected : boolean = false

  constructor() { }

  @HostListener('click') onClick(){
    this.isSelected = !this.isSelected;
    this.isSelected ? this.billing = '#F8DE7E' : this.billing = ''
  }
}
