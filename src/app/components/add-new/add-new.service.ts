import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Injectable} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SharedService} from "../shared.service";

@Injectable()

export class AddNewService {

  constructor( private asf : AngularFirestore,
               private fb : FormBuilder,
               private sharedService : SharedService) { }

  //add new activity
  addNewActivity( form : FormGroup ){
    const newActivity = {
      description: form.get("description")?.value,
      start: form.get("startTime")?.value,
      end: form.get("endTime")?.value,
      isBillable: form.get("isBillable")?.value
    }

    this.asf.collection('test').add(newActivity).then(res => {
      //add Primeng toast
      this.sharedService.successfulToast('Time Entry Has Been Created');
    }).catch( error => {
      //add Primeng toast
      this.sharedService.errorToast(error.message);
    });
  }

  //initialization of form
  initForm(){
    const endTime = new Date (new Date().getTime() + (3600 * 1000))

    return this.fb.group({
      description: ['', Validators.required],
      startTime: new Date(),
      endTime: endTime,
      activeTime: '',
      isBillable: false
    })
  }

  timerLogic( form : FormGroup, val : number ): FormGroup{
    form.get("startTime")?.value.setSeconds(0);
    form.get("endTime")?.value.setSeconds(0);
    form.get("activeTime")?.setValue(new Date((val * 1000) - (3600 * 1000)));
    const start = form.get("startTime")?.value.getTime();
    form.get("endTime")?.setValue(new Date(start + (val * 1000)));
    return form;
  }
}
