import {Component, Input, OnInit} from '@angular/core';
import {Activity} from "../../models/activity";
import {FormBuilder, FormGroup} from "@angular/forms";
import {SharedService} from "../../shared.service";
import {ActivityListService} from "../activity-list.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
  providers: [SharedService,
              MessageService]
})
export class EditComponent implements OnInit {
  @Input() activity! : Activity;
  editForm! : FormGroup;
  activeTime! : Date;

  constructor( private fb : FormBuilder,
               private sharedService : SharedService,
               private activityListService : ActivityListService ) { }

  ngOnInit(): void {
    this.editForm = this.fb.group({
      description: this.activity.description,
      startTime: this.activity.start.toDate(),
      endTime: this.activity.end.toDate(),
      activeTime: '',
      isBillable: this.activity.isBillable
    })

    this.activeTime = this.sharedService.countActiveTime(this.editForm);
    this.editForm.get("activeTime")?.setValue(this.activeTime);
  }

  onUpdate(){
    this.activityListService.updateActivity(this.editForm, this.activity.activityId);
  }

  onSetActiveTime(){
    const newEndTime = this.sharedService.onChangeActiveTime(this.editForm);
    this.editForm.get("endTime")?.setValue(newEndTime);
  }

  countDifference(){
    const difference = this.sharedService.countActiveTime(this.editForm);
    this.editForm.get("activeTime")?.setValue(difference);
  }

  onBillable(){
    const isBillable = this.editForm.get("isBillable")?.value;
    this.editForm.get("isBillable")?.setValue(!isBillable);
  }
}
