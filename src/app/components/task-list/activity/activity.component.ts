import {Component, Input, OnInit} from '@angular/core';
import {Activity} from "../../models/activity";
import {ActivityListService} from "../activity-list.service";

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  @Input() activity! : Activity;

  constructor( private activityListService : ActivityListService ) { }

  ngOnInit(): void {
  }

  //delete activity
  onDelete( id : string ){
    this.activityListService.deleteActivity(id);
  }

}
