import { Pipe, PipeTransform } from '@angular/core';
import {Activity} from "../models/activity";

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(activities : Activity[]|null, filterInput : string): Activity[]|null {
    const newArr : Activity[] = [];
    activities?.filter( item => {
      if( item.description.toLocaleLowerCase().indexOf(filterInput.toLocaleLowerCase()) > -1) {
        newArr.push(item);
      }
    })
    return newArr;
  }

}
