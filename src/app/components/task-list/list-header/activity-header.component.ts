import {Component, Input, OnInit} from '@angular/core';
import {Activity} from "../../models/activity";
import {ActivityListService} from "../activity-list.service";

@Component({
  selector: 'app-list-header',
  templateUrl: './activity-header.component.html',
  styleUrls: ['./activity-header.component.css'],
  providers: []
})
export class ActivityHeaderComponent implements OnInit {
  @Input() activity! : Activity;
  dateOfCreation! : Date;
  activeTime! : any;
  isTodayActivity! : boolean;

  constructor( private activityListService : ActivityListService ) { }

  ngOnInit(): void {
    this.dateOfCreation = this.activity.start.toDate();
    this.activeTime = this.countActiveTime();
    this.isTodayActivity = this.activityListService.checkIfToday( this.dateOfCreation );
  }

  //count difference between start date and end date
  private countActiveTime(){
    return ( (this.activity.end.toDate().getTime() - this.dateOfCreation.getTime()) - (3600*1000) );
  }
}
