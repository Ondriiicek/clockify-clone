import { Injectable } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {SharedService} from "../shared.service";

@Injectable()
export class ActivityListService {

  constructor(private sharedService: SharedService,
              private fb: FormBuilder,
              private afs: AngularFirestore) {
  }

  //check if start date is today date for list-header
  checkIfToday(dateOfCreation: Date): boolean {
    const today = new Date().setHours(0, 0, 0, 0);
    const taskDate = dateOfCreation.setHours(0, 0, 0, 0);

    if (taskDate === today) {
      return true;
    }
    return false;
  }

  //initiliazing form
  initForm(): FormGroup {
    return this.fb.group({
      description: '',
      startTime: '',
      endTime: ''
    })
  }

  //delete activity
  deleteActivity(id: string) {
    this.afs.collection("test").doc(id).delete().then(res => {
      this.sharedService.successfulToast('Time Entry Has Been Deleted');
    }).catch(error => {
      this.sharedService.errorToast(error.message);
    })
  }

  //update activity
  updateActivity(form: FormGroup, id: string) {
    const updatedActivity = {
      description: form.get("description")?.value,
      start: form.get("startTime")?.value,
      end: form.get("endTime")?.value,
      isBillable: form.get("isBillable")?.value
    }
    this.afs.collection("test").doc(id).update(updatedActivity).then(res => {
      this.sharedService.successfulToast('Time Entry Has Been Updated');
    }).catch(error => {
      this.sharedService.errorToast(error.message);
    })
  }
}
