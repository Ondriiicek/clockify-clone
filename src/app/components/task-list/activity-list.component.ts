import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/compat/firestore";
import {Observable} from "rxjs";
import {ActivityListService} from "./activity-list.service";
import {MessageService} from "primeng/api";
import { FormGroup} from "@angular/forms";
import {Activity} from "../models/activity";
import {SharedService} from "../shared.service";

@Component({
  selector: 'app-task-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css'],
  providers: [ ActivityListService,
               MessageService,
               SharedService]
})
export class ActivityListComponent implements OnInit {
  @Input() filterInput! : string;
  activities$! : Observable<Activity[]>;
  activityCollection! : AngularFirestoreCollection<Activity>;
  listActivityForm! : FormGroup;
  isEmpty! : boolean;

  constructor( private asf : AngularFirestore,
               private activityListService : ActivityListService) { }

  ngOnInit(): void {
    //sort data by date
    this.activityCollection = this.asf.collection('test', ref =>{
        return ref.limit(5).orderBy("start", "desc");
      }
    )
    this.activities$ = this.activityCollection.valueChanges( {idField: 'activityId'} );

    //initiliazing form
    this.listActivityForm = this.activityListService.initForm();

  }
}
