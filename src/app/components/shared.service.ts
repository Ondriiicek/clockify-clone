import { Injectable } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {MessageService} from "primeng/api";

@Injectable()

export class SharedService {

  constructor( private messageService : MessageService ) { }

  //count difference between start date and end date
  countActiveTime( form : FormGroup ) : Date{
    const startTime = form.get("startTime")?.value;
    const endTime = form.get("endTime")?.value;
    const difference = (endTime.getTime() - startTime.getTime()) - (3600 * 1000);
    return new Date(difference);
  }


  onChangeActiveTime( form : FormGroup ){
    const activeTime = form.get("activeTime")?.value.getTime();
    const startTime = form.get("startTime")?.value.getTime();
    const newEndTime = new Date(startTime + activeTime + (3600 * 1000) );
    return newEndTime;
  }

  //Successful Primeng Toast
  successfulToast( message : string ) {
    this.messageService.add({severity:'success', summary: message, detail:''});
  }

  //Primeng toast for error
  errorToast( error : string ) {
    this.messageService.add({severity:'error', summary: error, detail:''});
  }
}
