export interface Activity {
  description : string;
  start : any;
  end : any;
  activityId : string;
  isBillable : boolean;
}
